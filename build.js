const minify = require("minify");
const fs = require("fs-extra");

fs.ensureDirSync("dist");
fs.emptyDirSync("dist");

fs.copySync("index.html", "dist/index.html");

minify("dist/index.html").then((o) => fs.outputFileSync("dist/index.html", o));
